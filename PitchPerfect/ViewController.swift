//
//  ViewController.swift
//  PitchPerfect
//
//  Created by Aleksey Kabishau on 0715..17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: Properties
    @IBOutlet weak var recordingLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }


    //MARK: Methods
    @IBAction func recordAudio(_ sender: Any) {
        print("Record button is pressed")
        recordingLabel.text = "Recording in Progress"
    }
    
    @IBAction func stopRecording(_ sender: Any) {
        print("Stop button pressed")
        recordingLabel.text = "Tap to record"
    }
}

